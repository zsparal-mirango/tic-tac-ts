import React from "react";
import { Store } from "redux";
import { Provider } from "react-redux";

import { AppState } from "./store";

import App from "./ui/App";

interface Props {
  store: Store<AppState>;
}

export class Root extends React.Component<Props> {
  render() {
    const { store } = this.props;
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

export default Root;
