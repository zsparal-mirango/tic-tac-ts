import React from "react";
import ReactDOM from "react-dom";
import Root from "./Root";

import "bulma/css/bulma.min.css";
import "./index.css";
import configureStore from "./store/configureStore";

const store = configureStore();

const root = document.getElementById("root");
function renderApplication() {
  ReactDOM.render(<Root store={store} />, root);
}

renderApplication();

if (process.env.NODE_ENV === "development" && module.hot) {
  module.hot.accept(["./Root"], renderApplication);
}
