import { Store, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";

import { AppState, rootReducer } from "./index";

export default (initialState?: AppState): Store<AppState> => {
  const enhancers = composeWithDevTools();

  const store = initialState
    ? createStore(rootReducer, initialState, enhancers)
    : createStore(rootReducer, enhancers);

  if (process.env.NODE_ENV === "development" && module.hot) {
    module.hot.accept(["./index"], () => {
      store.replaceReducer(rootReducer);
    });
  }

  return store;
};
