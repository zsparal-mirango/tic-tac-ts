export interface CounterState {
  counter: number;
}

export enum ActionTypes {
  Increase = "counter/Increase",
  Decrease = "counter/Decrease"
}

export interface IncreaseAction {
  type: ActionTypes.Increase;
  payload: { valueToAdd: number };
}

export interface DecreaseAction {
  type: ActionTypes.Decrease;
  payload: { valueToSubtract: number };
}

export type CounterAction = IncreaseAction | DecreaseAction;

export const increase = (valueToAdd: number): IncreaseAction => ({
  type: ActionTypes.Increase,
  payload: { valueToAdd }
});

export const decrease = (valueToSubtract: number): DecreaseAction => ({
  type: ActionTypes.Decrease,
  payload: { valueToSubtract }
});

const initialState: CounterState = {
  counter: 0
};

export function reducer(state = initialState, action: CounterAction): CounterState {
  switch (action.type) {
    case ActionTypes.Increase:
      return {
        ...state,
        counter: state.counter + action.payload.valueToAdd
      };
    case ActionTypes.Decrease:
      return {
        ...state,
        counter: state.counter - action.payload.valueToSubtract
      };
    default:
      return state;
  }
}
