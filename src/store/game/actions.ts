import { action, CreateActionType } from "../util";

export enum ActionTypes {
  PlayNext = "game/PlayNext",
  Reset = "game/Reset"
}

export const playNext = (row: number, col: number) =>
  action({ type: ActionTypes.PlayNext, payload: { row, col } });
export const reset = () => action({ type: ActionTypes.Reset, payload: {} });

import * as actions from "./actions";
export type GameAction = CreateActionType<typeof actions>;
