import { Player, SquareValue, switchPlayer, indexFor } from "../model";
import { GameAction, ActionTypes } from "./actions";

export interface GameState {
  currentPlayer: Player;
  board: SquareValue[];
  winner: Player | null;
}

export const initialState: GameState = {
  currentPlayer: "X",
  board: [null, null, null, null, null, null, null, null, null],
  winner: null
};

export function reducer(state = initialState, action: GameAction): GameState {
  switch (action.type) {
    case ActionTypes.PlayNext:
      const { row, col } = action.payload;
      const index = indexFor(row, col);

      if (!isValidMove(state.winner, state.board, index)) {
        return state;
      }

      const board = [
        ...state.board.slice(0, index),
        state.currentPlayer,
        ...state.board.slice(index + 1)
      ];
      return {
        ...state,
        board,
        currentPlayer: switchPlayer(state.currentPlayer),
        winner: findWinningPlayer(board)
      };
    case ActionTypes.Reset:
      return initialState;
    default:
      return state;
  }
}

const findWinningPlayer = (board: SquareValue[]): Player | null => {
  return checkWinningRows(board) || checkWinningCols(board) || checkWinningDiagonals(board);
};

const isValidMove = (winner: Player | null, board: SquareValue[], index: number) => {
  return !winner && !board[index];
};

const checkWinningRows = (board: SquareValue[]): Player | null => {
  for (let row = 0; row < board.length; row += 3) {
    if (!!board[row] && board[row] === board[row + 1] && board[row] === board[row + 2]) {
      return board[row];
    }
  }

  return null;
};

const checkWinningCols = (board: SquareValue[]): Player | null => {
  for (let col = 0; col < 3; col++) {
    if (!!board[col] && board[col] === board[col + 3] && board[col] === board[col + 6]) {
      return board[col];
    }
  }

  return null;
};

const checkWinningDiagonals = (board: SquareValue[]): Player | null => {
  if (!board[4]) {
    return null;
  }

  if (board[0] === board[4] && board[4] === board[8]) {
    return board[4];
  }

  if (board[2] === board[4] && board[2] == board[6]) {
    return board[4];
  }

  return null;
};

export * from "./actions";
