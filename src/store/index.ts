import { combineReducers } from "redux";

import * as counter from "./counter";
import * as game from "./game";

export interface AppState {
  counter: counter.CounterState;
  game: game.GameState;
}

export const rootReducer = combineReducers<AppState>({
  counter: counter.reducer,
  game: game.reducer
});
