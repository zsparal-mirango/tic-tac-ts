export type Player = "X" | "O";
export type SquareValue = Player | null;

export function switchPlayer(player: Player): Player {
  return player === "X" ? "O" : "X";
}

export const indexFor = (row: number, col: number) => {
  return row * 3 + col;
};
