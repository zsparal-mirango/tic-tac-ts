interface Action<T extends string> {
  type: T;
}

export function action<T extends string, A extends Action<T>>(action: A) {
  return action;
}

export type CreateActionType<A> = {
  [K in keyof A]: A[K] extends (...args: any[]) => infer R ? R : never
}[keyof A];
