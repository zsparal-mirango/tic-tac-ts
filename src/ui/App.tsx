import React from "react";
import { Container, Title } from "bloomer";

import Board from "./TicTacToe/Board";
import Counter from "./Counter";

class App extends React.Component {
  render() {
    return (
      <Container>
        <Title>Counter</Title>
        <Counter />

        <Title>Tic-Tac-Toe</Title>
        <Board />
      </Container>
    );
  }
}

export default App;
