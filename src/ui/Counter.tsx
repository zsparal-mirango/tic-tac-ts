import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Box, Button, Content } from "bloomer";

import { AppState } from "../store";
import { increase, decrease } from "../store/counter";

interface Props {
  counter: number;
  increase(value: number): void;
  decrease(value: number): void;
}

export class Counter extends React.Component<Props> {
  render() {
    const { counter } = this.props;
    return (
      <Content>
        <Box>
          <p>Counter: {counter}</p>
          <Button onClick={this.onDecrease}>-</Button>
          <Button onClick={this.onIncrease}>+</Button>
        </Box>
      </Content>
    );
  }

  private onIncrease = () => {
    this.props.increase(1);
  };

  private onDecrease = () => {
    this.props.decrease(1);
  };
}

export default connect(
  (state: AppState) => ({
    counter: state.counter.counter
  }),
  dispatch => bindActionCreators({ increase, decrease }, dispatch)
)(Counter);
