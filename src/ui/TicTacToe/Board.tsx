import React from "react";
import { connect } from "react-redux";
import { Button } from "bloomer";

import { Player, indexFor, SquareValue } from "../../store/model";
import Square from "./Square";
import { Title } from "bloomer/lib/elements/Title";

import "./Board.css";
import { AppState } from "../../store";
import { bindActionCreators } from "redux";
import { playNext, reset } from "../../store/game";

interface Props {
  board: SquareValue[];
  currentPlayer: Player;
  winner: Player | null;

  playNext(row: number, col: number): void;
  reset(): void;
}

export class Board extends React.Component<Props> {
  render() {
    const { board, currentPlayer, winner, playNext, reset } = this.props;
    return (
      <div>
        <Title className="winner-title" isSize={3}>
          {winner && (
            <>
              The winner is: <span className={`winner ${winner}`}>{winner}</span>
            </>
          )}
        </Title>
        {[...Array(3).keys()].map(row => (
          <div key={row} className="row">
            {[...Array(3).keys()].map(col => (
              <Square
                key={indexFor(row, col)}
                value={board[indexFor(row, col)]}
                previewValue={currentPlayer}
                row={row}
                col={col}
                onClicked={playNext}
              />
            ))}
          </div>
        ))}
        <Button className="reset" onClick={reset}>
          Reset
        </Button>
      </div>
    );
  }
}

export default connect(
  (state: AppState) => ({
    board: state.game.board,
    currentPlayer: state.game.currentPlayer,
    winner: state.game.winner
  }),
  dispatch => bindActionCreators({ playNext, reset }, dispatch)
)(Board);
