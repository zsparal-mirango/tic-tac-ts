import React from "react";
import { Button } from "bloomer";

import { SquareValue, Player } from "../../store/model";
import "./Square.css";

interface Props {
  row: number;
  col: number;
  value: SquareValue;
  previewValue: Player;

  onClicked(row: number, col: number): void;
}

interface State {
  showPreview: boolean;
}

export class Square extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { showPreview: false };
  }

  render() {
    const { value, previewValue } = this.props;
    const { showPreview } = this.state;

    const previewClass = !value && showPreview ? " preview" : "";
    const displayedValue = value || (showPreview && previewValue) || "";
    return (
      <Button
        className={`square${previewClass} ${displayedValue}`}
        onClick={this.onClicked}
        onMouseEnter={this.togglePreview}
        onMouseLeave={this.togglePreview}
      >
        {displayedValue}
      </Button>
    );
  }

  private onClicked = () => {
    const { col, row, onClicked } = this.props;
    return onClicked(row, col);
  };

  private togglePreview = () => {
    this.setState(state => ({ showPreview: !state.showPreview }));
  };
}

export default Square;
