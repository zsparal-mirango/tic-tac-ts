export type Player = "X" | "O";
export type SquareValue = Player | null;

export function switchPlayer(player: Player): Player {
  return player === "X" ? "O" : "X";
}
